<?php
	// prevent direct access
	defined( 'ABSPATH' ) or die( 'Nope ><' );

	/**
	 * Contains the majority of the plugin code
	 */
	class UserTable{
		/**
		 * [init entry point. registers actions etc.]
		 */
		public static function init(){
			// register the scripts/styles
			add_action( 'wp_enqueue_scripts', 'UserTable::add_scripts' );
			// register the shortcode
			add_action( 'init', 'UserTable::register_shortcode' );
			// register ajax stuff
			add_action( 'wp_ajax_usertable', 'UserTable::ajax_list_users' );
		}
		/**
		 * [add_scripts adds the script/style of the plugin]
		 */
		public static function add_scripts(){
			// add the js
			wp_register_script( 'usertable', CT_PLUGIN_URL . 'build/js/build.js' );
			wp_enqueue_script( 'usertable', CT_PLUGIN_URL . 'build/js/build.js' );
			// add the css
			wp_register_style( 'usertable', CT_PLUGIN_URL . 'build/css/build.css' );
			wp_enqueue_style( 'usertable', CT_PLUGIN_URL . 'build/css/build.css' );
			// expose ajax url to FE
			wp_localize_script( 'usertable', 'ajax_url', admin_url( 'admin-ajax.php' ) );
		}
		/**
		 * [register_shortcode add the shortcode]
		 */
		public static function register_shortcode(){
			// add the usertable shortcode. use like: [usertable]
			add_shortcode( 'usertable', 'UserTable::render_shortcode' );
		}
		/**
		 * [is_admin internal method to determine if the logged in user is an administrator or not]
		 * @return boolean
		 */
		private static function is_admin(){
			return is_user_logged_in() && in_array('administrator', wp_get_current_user()->roles);
		}
		/**
		 * [render_shortcode renders the shortcode html for the FE]
		 */
		public static function render_shortcode(){
			?>
			<div class="usertable">
				<?php
				// if the admin is logged in
				if( UserTable::is_admin() ):
					// get the users
					$users = get_users();
				?>
					<h1 class="usertable-header"><?php _e('Usertable', 'usertable'); ?></h1>
					<form class="usertable-filtersort">
						<label for="ut_filter" class="usertable-filtersort-label"><?php _e('Filter by:', 'usertable'); ?> </label>
						<select id="ut_filter" class="usertable-filtersort-select">
							<option value="all"><?php _e('All roles', 'usertable'); ?></option>
							<?php
								// get the roles from the users
								$available_roles = array_unique(array_map(function($user){
									return array_values($user->roles)[0];
								}, $users));
								// output the available roles
								foreach( $available_roles as $role ): ?>
									<option value="<?php echo strtolower($role); ?>"><?php echo ucfirst($role); ?></option>
							<?php
								endforeach;
							?>
						</select>
						<label for="ut_sort" class="usertable-filtersort-label"><?php _e('Sort by:', 'usertable'); ?> </label>
						<select id="ut_sort" class="usertable-filtersort-select">
							<option value="display_name_az"><?php _e('Display name (A-Z)', 'usertable'); ?></option>
							<option value="display_name_za"><?php _e('Display name (Z-A)', 'usertable'); ?></option>
							<option value="username_az"><?php _e('User name (A-Z)', 'usertable'); ?></option>
							<option value="username_za"><?php _e('User name (Z-A)', 'usertable'); ?></option>
						</select>
					</form>
					<table class="usertable-table">
						<tr class="usertable-table-row">
							<th class="usertable-table-row-cell"><?php _e('Username', 'usertable'); ?></th>
							<th class="usertable-table-row-cell"><?php _e('Display name', 'usertable'); ?></th>
							<th class="usertable-table-row-cell"><?php _e('Role', 'usertable'); ?></th>
						</tr>
						<?php
						// build the table rows below the header row
						foreach( array_slice($users, 0, 10) as $user ):
						?>
						<tr class="usertable-table-row">
							<td class="usertable-table-row-cell"><?php echo $user->data->user_login; ?></td>
							<td class="usertable-table-row-cell"><?php echo $user->data->display_name; ?></td>
							<td class="usertable-table-row-cell"><?php echo array_values($user->roles)[0] ?></td>
						</tr>
						<?php
						endforeach;
						?>
					</table>
					<?php
					// render pagination if there's more than 10 users
					if( count($users) > 10 ): ?>
						<ul class="usertable-pagination">
							<?php foreach( range(1, ceil(count($users) / 10)) as $page ): ?>
								<li class="usertable-pagination-item<?php echo $page == 1 ? ' usertable-pagination-item--active' : ''; ?>" value="<?php echo $page; ?>"><?php echo $page ?></li>
							<?php endforeach; ?>
						</ul>
					<?php
					endif;
					?>
				<?php
				// user is not admin & logged in so render the nope
				else:
				?>
					<h3 class="usertable-nope"><?php _e("You're not authorized to access this content. Contact your site admin.", 'usertable') ?></h3>
				<?php
				endif;
				?>
			</div>
			<?php
		}
		public static function ajax_list_users(){
			if( UserTable::is_admin() ){
				/**
				 * $_POST = array(
				 * 		'page' => number,
				 * 		'filter' => string (list of roles),
				 * 		'sort' => string (display_name_az, username_az, display_name_za, username_za)
				 * );
				 */

				// sanitize inputs
				$page = intval($_POST['page']);
				$filter = sanitize_text_field( $_POST['filter'] );
				$sort = sanitize_text_field( $_POST['sort'] );

				// set param defaults when they're empty
				if( empty($page) ) $page = 1;
				if( empty($filter) ) $filter = 'all';
				if( empty($sort) ) $sort = 'display_name_az';


				// try the cache for users
				$users = wp_cache_get('ut_users');
				// were there users in the cache?
				if( empty( $users ) ){
					// get the users
					$users = get_users();
					// store the users in the cache
					wp_cache_set( 'ut_users', $users );
				}

				// get the total pages
				$total_pages = ceil(count($users) / 10);

				// bail if page is higher than the available pages
				if( $page > $total_pages ){
					echo 0;
					wp_die();
				}

				// begin sorting
				switch( substr($sort, 0, -3) ) {
					// if sorting by display name
					case 'display_name':
						usort($users, function($a, $b){
							return strcmp($a->data->display_name, $b->data->display_name);
						});
						if( strpos($sort, '_za') ){
							$users = array_reverse($users);
						}
						break;
					// if sorting by username
					case 'username':
						usort($users, function($a, $b){
							return strcmp($a->data->user_login, $b->data->user_login);
						});
						if( strpos($sort, '_za') ){
							$users = array_reverse($users);
						}
						break;
					// bail if using unsupported sort
					// ...maybe log/block ip?
					default:
						echo 0;
						wp_die();
						break;
				}

				// begin filtering
				$users = array_map(function($user) use($filter) {
					// do the filtering
					if( $filter !== 'all' && in_array($filter, array_values($user->roles)) ){
						return $user;
					}
					// return everything if the filter is set to all
					elseif( $filter == 'all' ){
						return $user;
					}
				}, $users);

				// strip null values
				$users = array_values(array_filter($users));
				// make new total pages
				$new_total_pages = ceil(count($users) / 10);
				// deliver page
				$users = array_slice($users, ($page - 1) * 10, 10);

				// deliver json_encoded users to FE
				echo json_encode( array(
					'users' => $users,
					'totalPages' => $new_total_pages,
				) );
			}
			else{
				echo 0;
			}
			wp_die();
		}
	}
?>