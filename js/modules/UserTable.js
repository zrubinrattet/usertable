import $ from 'jquery';
/**
 * [UT the main module of the UserTable plugin. controls the UI, state, event handling & ajax methods of the UT]
 * @type {Object}
 */
let UT = {
	/**
	 * [ui store the ui elements to listen to]
	 * @type {Object}
	 */
	ui : {},
	/**
	 * [state holds the state of the usertable system]
	 * @type {Object}
	 */
	state : {
		// default values...
		page : 1,
		sort : 'display_name_az',
		filter : 'all',
		// totalPages is used to populate the numbered pagination
		totalPages : 0,
	},
	/**
	 * [_init entry point]
	 * @return {[type]} [description]
	 */
	_init : () => {
		// nothing to fire on init so head to ready
		$(document).ready(UT._ready);
	},
	/**
	 * [_ready fires when the document is "ready"]
	 */
	_ready : () => {
		// setup the ui
		UT._setUI();

		// listen to the filter/sort ui
		UT.ui.sort.on('change', UT._updateState.bind(null, 'sort'));
		UT.ui.filter.on('change', UT._updateState.bind(null, 'filter'));
	},
	/**
	 * [_setUI sets up the UI of the usertable]
	 */
	_setUI : () => {
		// set the ui object
		UT.ui = {
			pagination : $('.usertable-pagination-item'),
			sort : $('.usertable-filtersort-select#ut_sort'),
			filter : $('.usertable-filtersort-select#ut_filter'),
		};
		// set the total pages
		UT.state.totalPages = UT.ui.pagination.length

		// is there pagination?
		if( UT.state.totalPages != 0 ){
			// unbind event listeners just in case
			UT.ui.pagination.off('click');
			// listen to the pagination
			UT.ui.pagination.on('click', UT._updateState.bind(null, 'page'));
		}
	},
	/**
	 * [_updateState updates the state of the UI]
	 * @param  {string} element the UI element that was interacted with
	 * @param  {object} event   the event object
	 */
	_updateState : (element, event) => {
		// always set the page back to 1 when playing with filter/sort
		if( element !== 'page' ){
			UT.state.page = 1;
		}

		// if role filter is changed, set the sort to the default value
		if( element === 'filter' ){
			UT.state.sort = 'display_name_az';
			UT.ui.sort.val('display_name_az');
		}

		// update the state of the element
		UT.state[element] = event.currentTarget.value;
		
		// remove the active class from the pagination element
		UT.ui.pagination.removeClass('usertable-pagination-item--active');
		// set the active class based on the page that was clicked on
		$($('.usertable-pagination-item')[UT.state.page - 1]).addClass('usertable-pagination-item--active');

		// set the data passed to ajax post
		let data = {
			action : 'usertable',
			page : UT.state.page,
			sort : UT.state.sort,
			filter : UT.state.filter,
		};
		// get users based on updated UI state
		$.post(ajax_url, data, UT._renderResponse);
	},
	/**
	 * [_renderResponse updates the UI after the ajax call]
	 * @param  {mixed} res the response from the server
	 */
	_renderResponse : (res) => {
		// if the response isn't bunk
		if( res != 0 ){
			// convert response to obj
			res = JSON.parse(res);
			// did the total pages change?
			if( res.totalPages != UT.state.totalPages ){
				// set the total pages state
				UT.state.totalPages = res.totalPages;
				// remove the old pagination ui elements
				UT.ui.pagination.remove();
				// loop through total pages
				for( let i = 1; i <= UT.state.totalPages; i++ ){
					// start building the element
					let newEl = '<li class="usertable-pagination-item';
					// add the active class to the first element
					if( i == 1 ){
						newEl += ' usertable-pagination-item--active';
					}
					// finish up the element
					newEl += '" value="' + i + '">' + i + '</li>';
					// add the element to the pagination container
					$('.usertable-pagination').append(newEl);
				}
			}

			// we got users right?
			if( res.users.length > 0 ){
				// remove the current users
				$('.usertable-table-row:not(:first-child)').remove();
				// loop through the users
				res.users.forEach((user) => {
					// build new user row
					let newEl = `
						<tr class="usertable-table-row">
							<td class="usertable-table-row-cell">${user.data.user_login}</td>
							<td class="usertable-table-row-cell">${user.data.display_name}</td>
							<td class="usertable-table-row-cell">${Object.values(user.roles)[0]}</td>
						</tr>
					`;
					// add the user row to the table
					$('.usertable-table').append(newEl);
				});
			}
			// update the UI store
			UT._setUI();
		}
	},
};

export default UT;