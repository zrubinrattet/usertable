import UserTable from './modules/UserTable.js';

let C_UT = {
	modules : [
		UserTable,
	],
	_init : () => {
		C_UT.modules.forEach( (module) => {
			module._init();
		} );
	}
};

C_UT._init();