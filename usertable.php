<?php
/**
 * Plugin Name: UserTable
 * Description: Creates a shortcode that outputs an interactive table of users visible only to admins
 * Version: 0.0.1
 * Author: Zach Rubin-Rattet
 */

// define CT_PLUGIN_PATH & CT_PLUGIN_URL consts
if( !defined('CT_PLUGIN_PATH') ){
    define('CT_PLUGIN_PATH', plugin_dir_path( __FILE__ ));
}
if( !defined('CT_PLUGIN_URL') ){
    define('CT_PLUGIN_URL', plugin_dir_url( __FILE__ ));
}

// bring in the main class
require_once 'class-usertable.php';
// blast off
UserTable::init();
?>